#Lab 1 - Creating the Cluster in GCP and setting up Gitlab

For the labs in todays workshop, we will be using Google's cloud environment to run services in Kubernetes.
You will need to create an account, or use an existing account for todays exercise. Free trials are available which
give $300 in free credit to use.

We will also be using Gitlab as our code repository and CI/CD system.  

## Part 1 - Google Cloud Setup

### Access to GCP
First, lets setup a gcp user and project - https://cloud.google.com/

1. Creating a new account will give you $300 credit
2. Unfortunately, you will have to setup a billing account to use some of the API's.
3. You will need to relax some quotas which will require upgrading your account.
You can close your account after the workshop to avoid spending any money. You can do this by navigating to
IAM & admin > Quotas and either editing the quota, or upgrading from the free tier. You should have 24 CPUs in
us-central-1 after updating your quota.
4. Finally, Create a project to work with in GCP.

### API Permissions

We will also need to enable a few API's to allow Gitlab to interact with Kubernetes. Navigate to API's and Services
and enable the following APIs
* Google Kubernetes Engine API
* Cloud Resource Manager API
* Cloud Billing API
* Cloud Runtime Configuration API

### Service Account Permissions

You must grant your default compute service account
the correct permissions before creating the deployment.
Otherwise, the installation will fail. Make sure that your
default compute service account (by default
`[PROJECT_NUMBER]-compute@developer.gserviceaccount.com`)
includes the following roles:
* `roles/container.admin` (Kubernetes Engine Admin)
* `roles/editor` (included by default)

You can set this permission by navigating to the [IAM
section](https://console.cloud.google.com/permissions/projectpermissions)
of the Google Cloud Console, viewing the permissions for your
default compute service account
(`[PROJECT_NUMBER]-compute@developer.gserviceaccount.com`), and
making sure that both Editor (`roles/editor`) and Kubernetes
Engine Admin (`roles/container.admin`) are selected.

### Installing Google Cloud SDK & Kubectl

We will also need to work with the gcloud / kubectl cli tools for the labs in todays workshop. Here are some instructions
on installing

* [Install Google Cloud SDK](https://cloud.google.com/sdk/install)
* [Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Tip: you can use [gcloud init] to initialize some defaults for the google cloud sdk


## Part 2 - Gitlab Setup

Now, if you haven't already done so, lets set up a gitlab.com account - https://gitlab.com/users/sign_in#register-pane

Once you've done that, fork the following project - https://gitlab.com/michael.patel.nodeconf/gs-nodeconf-workshop

This has some of the templates you will be using for the lab exercises today.


## Part 3 - Creating the Cluster


### Cluster Provisioning
Ok, now we should be ready to create the cluster! Notice in the lab, there is a folder 'cluster' which
contains the deployment manager template we will be using to create our cluster. Using gcloud
enter the following command (note: it is assumed you are running in the lab-1 directory)

```
$ gcloud deployment-manager deployments create nodeconf-cluster-deployment --config=../deployments/cluster/istio-cluster.yaml
```

This command will take some time to finish, when it completes, you should see the following printed in console:

```
Create operation operation-1560791721599-58b88242260cd-7c4729ca-fc3e898c completed successfully.
NAME                        TYPE                          STATE      ERRORS  INTENT
gs-nodeconf-ci              container.v1.cluster          COMPLETED  []
nodeconf-my-cluster-config  runtimeconfig.v1beta1.config  COMPLETED  []
nodeconf-my-cluster-vm      compute.v1.instance           COMPLETED  []
nodeconf-my-cluster-waiter  runtimeconfig.v1beta1.waiter  COMPLETED  []
```

### Configuring Kubectl

Now we have a cluster, lets make sure our kubectl is using it. Run the following command in google cloud sdk

```
gcloud container clusters get-credentials gs-nodeconf-ci --zone=us-central1-a
```

now try to run

```
kubectl get pod -n istio-system
```

You should get the following output

```
NAME                                      READY   STATUS      RESTARTS   AGE
grafana-7f4d444dd5-xl84p                  1/1     Running     0          26h
istio-citadel-7dbf78bf8f-q4g8p            1/1     Running     0          26h
istio-cleanup-secrets-1.1.8-dvxg5         0/1     Completed   0          26h
istio-galley-7f874545bd-n75zr             1/1     Running     0          26h
istio-grafana-post-install-1.1.8-fbh4k    0/1     Completed   0          26h
istio-ingressgateway-75479dbb99-gghcv     1/1     Running     0          26h
istio-init-crd-10-f7r7r                   0/1     Completed   0          26h
istio-init-crd-11-vcswq                   0/1     Completed   0          26h
istio-pilot-647dd8b4fd-d9686              2/2     Running     0          26h
istio-policy-6b5fbbb7bf-nfvzp             2/2     Running     2          26h
istio-security-post-install-1.1.8-mdtkb   0/1     Completed   0          26h
istio-sidecar-injector-5c77d99d8-pjsrp    1/1     Running     0          26h
istio-telemetry-5c9b4d6b95-6zttq          2/2     Running     2          26h
istio-tracing-79db5954f-w9qm5             1/1     Running     0          26h
kiali-68677d47d7-fm7kz                    1/1     Running     0          26h
prometheus-5977597c75-kwm8d               1/1     Running     0          26h
```
Note: If services are stuck in crash loops, it may be because permissions are not correctly configured. Please ask for help!

### Dashboards

There are a few supporting services we've deployed along with the cluster. These are
* Grafana *:15031* - Dashboards
* Kiali *:15029* - Observability (note: credentials are default set to admin/admin)
* Jaeger *:15032* - Tracing
* Prometheus *:15030* - Time Series Collection And Processing

We now need to set up a gateway so we can access these services. Run the following command to set up the gateway

```
kubectl apply -f dashboard.yaml -n istio-system
```
This will expose the services through our ingress controller. Lets check the ingress gateway to see what's accessible.
Try enter the following

```
kubectl get service istio-ingressgateway -n istio-system
```

You should get output similar to the below

```
NAME                   TYPE           CLUSTER-IP     EXTERNAL-IP     PORT(S)                                                                                                                                      AGE
istio-ingressgateway   LoadBalancer   10.11.251.71   35.222.138.58   15020:31918/TCP,80:31380/TCP,443:31390/TCP,31400:31400/TCP,15029:31349/TCP,15030:30957/TCP,15031:30468/TCP,15032:31772/TCP,15443:31980/TCP   27h
```
Take note of the EXTERNAL-IP, this will be how access dashboards. For example grafana can be accessed on http://35.222.138.58:15031

## Concluding Lab 1

You are now finished with Lab 1, in this lab we learned
* How to use the gcloud and kubectl tools to administer our Kubernetes cluster
* How to set up Kubernetes clusters in the cloud

In the next lab, we will look at [Testing and Deploying Services](../lab-2/README.md)
