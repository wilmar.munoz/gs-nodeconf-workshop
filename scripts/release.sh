#!/bin/sh
cd "services/$APP_NAME"
VERSION=$(echo $CI_COMMIT_REF_NAME | awk -F"_" '{print $1}')
docker build -t="us.gcr.io/${GCLOUD_PROJECT_ID}/${APP_NAME}:${VERSION}" .
docker push "us.gcr.io/${GCLOUD_PROJECT_ID}/${APP_NAME}:${VERSION}"