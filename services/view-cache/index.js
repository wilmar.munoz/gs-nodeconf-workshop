const express = require('express')
const request = require('request-promise-native')
const {readOrLoadData} = require('./read-through-cache')

const router = new express.Router()


router.get('/get-data', async (req, res)  => {
    const {key} = req.query
     const data = await  request.get('http://back-end:4001/data')
    res.setHeader('Content-Type', 'application/json')
    res.send(data)

})


router.get('/env', async (req, res)  => {
    const {key} = req.query
    // const data = await  request.get(`http://${key}/data`)
    res.setHeader('Content-Type', 'application/json')
    // res.send(data)
    res.send(JSON.stringify(process.env))
})


// awaits
/*
router.get('/get-data', async (req, res)  => {
    const {key} = req.query
    const data = await loadData(key, 'http://localhost:8001/sample.json')
    res.setHeader('Content-Type', 'application/json')
    res.send(data)
})

*/
//
// router.get('/get-data', (req, res) => {
//     const {key} = req.query
//     const data = readOrLoadData(key, 'https://raw.githubusercontent.com/tamingtext/book/master/apache-solr/example/exampledocs/books.json')
//     res.setHeader('Content-Type', 'application/json')
//     res.send(data)
// })


const server = express()

server.use(router)

server.listen(4000)